JRJ is a shutdown dialog written in Python. Provides a simple, customizable GUI interface to logoff, restart, or shutdown on the current device.
Currently tested on Windows 10 only.

----

## Customization Options
The user can configure the following items in the config.ini file:
- Window Title
- Window Dimensions
- Window Color
- Button Locations

The user can modify the following images in the /res/ folder
- Window Icon
- Window Background
- Logoff Image
	- Normal
	- Hover
	- Pressed
- Restart Image
	- Normal
	- Hover
	- Pressed
- Shutdown Image
	- Normal
	- Hover
	- Pressed

### Image Information
The image names must remain the same as they are provided.
Transparency is supported for all images.
The window icon must be a .ico file.
    To create a .ico file from a standard image file type, you can use the [RedKetchup Icon Converter](https://redketchup.io/icon-converter)