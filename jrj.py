# ----------------------------------------------------------------------------
# juiceboxxxxxx
#
# --|jrj.py|------------------------------------------------------------------
# jrj.py handles project wide variable creation, and GUI creation.
# ----------------------------------------------------------------------------

# --|Imports|-----------------------------------------------------------------
import tkinter as tk
import sys
from tkinter import ttk
from os.path import dirname, realpath
from os import system


# --|GLOBALS|-----------------------------------------------------------------
# ----|Directories|----
PROJDIR = dirname(realpath(__file__)) + "/"
RESDIR = "".join([PROJDIR, "res/"])

# ----|JRJ Items|----
STATES = ["normal", "hover", "pressed"]
CONFIG_ITEMS = ["window_title", "window_width",
                "window_height", "window_bg_color",
                "logoff_x", "logoff_y",
                "restart_x", "restart_y",
                "shutdown_x", "shutdown_y"]

# ----|Files|----
# Config File
CONFIG_FILE = "/".join([PROJDIR, "config.ini"])

# Window Images
IMG_WINDOW_ICON = "".join([RESDIR, "icon.ico"])
IMG_WINDOW_BG = "".join([RESDIR, "bg.png"])

# Power Control Images
IMG_LOG = ["".join([RESDIR,"logoff_",state,".png"]) for state in STATES]
IMG_RST = ["".join([RESDIR,"restart_",state,".png"]) for state in STATES]
IMG_SHD = ["".join([RESDIR,"shutdown_",state,".png"]) for state in STATES]

# All Files
FILES = ([CONFIG_FILE, IMG_WINDOW_ICON, IMG_WINDOW_BG] +
         IMG_LOG + IMG_RST + IMG_SHD)



# --|JRJ Class|---------------------------------------------------------------
class JRJ:
    """
    The JRJ class handles creation of the JRJ GUI.
    """
    
    # ----|Methods|-----------------------------------------------------------
    def __init__(self, config):
        """
        __init__ handles the initialization of a JRJ object according to the
        configuration items passed to it in the config dictionary.

        INPUT:
        config : dictionary
            Dictionary containing the user configuration of the window from
            the config.ini file in the project directory.
        """
        
        # ----|Initialize and Configure the Window|----
        # Init
        self.window = tk.Tk()
        self.window.geometry("x".join([config["window_width"],
                                       config["window_height"]]))
        self.window.resizable(False, False)
        self.window.config(bg = config["window_bg_color"])
        self.window.protocol("WM_DELETE_WINDOW", self.close)
        
        # Config
        self.window.title(config["window_title"])
        self.window.iconbitmap(IMG_WINDOW_ICON)
        
        
        # ----|Check for Errors|----
        if "error" in config.keys():
            self.dispErrors(config["error"])
            return
        
        
        # ----|Create GUI|----
        self.gui = dict()
        self.draw(config)
        
        # Start mainloop
        self.window.mainloop()
    
    
    def draw(self, config):
        """
        draw handles drawing the GUI elements of the JRJ window, and adds all
        created elements to the JRJ gui dictionary for access by later
        functions.
        
        gui dict structure:
            {"bg": Tkinter Canvas,
             "log": Tkinter Canvas Image ID,
             "rst": Tkinter Canvas Image ID,
             "shd": Tkinter Canvas Image ID}

        INPUT:
        config : dictionary
            Dictionary containing the user configuration of the window from
            the config.ini file in the project directory.
        """
        
        # ----|Create Canvas and Background|----
        # Canvas
        self.gui["bg"] = tk.Canvas(master = self.window,
                                   width = config["window_width"],
                                   height = config["window_height"])
        self.gui["bg"].configure(bg = config["window_bg_color"])
        self.gui["bg"].pack(expand = True, fill = "both")
        
        # Background
        self.bg = tk.PhotoImage(master = self.window,
                                file = IMG_WINDOW_BG)
        self.gui["bg"].create_image(0,0, anchor = "nw",
                                    image = self.bg, tags = "bg")
        
        
        # ----|Create Logoff, Restart, Shutdown Buttons|----
        # Create Images
        self.log = [tk.PhotoImage(master = self.window,
                                  file = img) for img in IMG_LOG]
        self.rst = [tk.PhotoImage(master = self.window,
                                  file = img) for img in IMG_RST]
        self.shd = [tk.PhotoImage(master = self.window,
                                  file = img) for img in IMG_SHD]
        
        # Create Buttons
        self.gui["log"] = self.gui["bg"].create_image(config["logoff_x"],
                                                      config["logoff_y"],
                                                      anchor = "nw",
                                                      image = self.log[0],
                                                      tags = ("log", "button"))
        self.gui["rst"] = self.gui["bg"].create_image(config["restart_x"],
                                                      config["restart_y"],
                                                      anchor = "nw",
                                                      image = self.rst[0],
                                                      tags = ("rst", "button"))
        self.gui["shd"] = self.gui["bg"].create_image(config["shutdown_x"],
                                                      config["shutdown_y"],
                                                      anchor = "nw",
                                                      image = self.shd[0],
                                                      tags = ("shd", "button"))
        
        # DEBUG
        # print("Logoff ID:", self.gui["log"], " Tags:", self.gui["bg"].gettags(self.gui["log"]),
        #       "\nRestart ID:", self.gui["rst"], " Tags:", self.gui["bg"].gettags(self.gui["rst"]),
        #       "\nShutdown ID:", self.gui["shd"], " Tags:", self.gui["bg"].gettags(self.gui["shd"]))
        # print(self.gui["bg"].find_withtag("button"))
        
        
        # ----|Bind Events|----
        self.state_switch = {"log": self.log,
                             "rst": self.rst,
                             "shd": self.shd}
        self.action_switch = {"log": self.logoff,
                              "rst": self.restart,
                              "shd": self.shutdown}
        
        
        for key in ["log", "rst", "shd"]:
            self.gui["bg"].tag_bind(self.gui[key],"<Leave>", self.normal)
            self.gui["bg"].tag_bind(self.gui[key], "<Enter>", self.hover)
            self.gui["bg"].tag_bind(self.gui[key], "<Button-1>", self.pressed)
            self.gui["bg"].tag_bind(self.gui[key],
                                    "<ButtonRelease-1>", self.hover)
    
    
    # --|Functionality Methods|-----------------------------------------------
    # ----|Mouse Location Methods|----
    def normal(self, e):
        """
        normal handles setting the button image to the normal state when the
        mouse is not over the button.

        INPUT:
        e : Tkinter event
            The event that triggers the method to be called.
        """
        
        # ----|Set all Buttons to Normal State|----
        # Get Button List
        buttonList = self.gui["bg"].find_withtag("button")
        
        # Set to normal state
        for button in buttonList:
            buttonType = self.gui["bg"].gettags(button)[0]
            image = self.state_switch[buttonType][0]
            self.gui["bg"].itemconfigure(button, image = image)
    
    
    def hover(self, e):
        """
        hover handles setting the button image to the hover state when the
        mouse is over the button.

        INPUT:
        e : Tkinter event
            The event that triggers the method to be called.
        """
        
        # Get button information
        buttonID = e.widget.find_closest(e.x, e.y, halo = 2)[0]
        buttonTag = self.gui["bg"].gettags(buttonID)[0]
        
        # Configure button to the hovered state
        self.gui["bg"].itemconfigure(buttonID,
                                     image = self.state_switch[buttonTag][1])
    
    
    def pressed(self, e):
        """
        pressed handles setting the button image to the pressed state when the
        left mouse button has been clicked on the button.

        INPUT:
        e : Tkinter event
            The event that triggers the method to be called.
        """
        
        # Get button information
        buttonID = e.widget.find_closest(e.x, e.y, halo = 2)[0]
        buttonTag = self.gui["bg"].gettags(buttonID)[0]
        
        # Configure button to the pressed stated
        self.gui["bg"].itemconfigure(buttonID,
                                     image = self.state_switch[buttonTag][2])
        
        # Trigger button's action
        self.action_switch[buttonTag]()
    
    
    # ----|Button Action Methods|----
    def logoff(self):
        """
        logoff handles logging off of the current user.
        """
        
        return system("shutdown -l")
    
    
    def restart(self):
        """
        restart handles restarting the computer.
        """
        
        return system("shutdown /r /t 1")
    
    
    def shutdown(self):
        """
        shutdown handles shutting down the computer
        """
        
        return system("shutdown /s /t 1")
        
    
    # ----|Helper Methods|----
    def dispErrors(self, message):
        """
        dispErrors displays any errors to the user that occurred during
        initial checks. This may be missing files, missing parameters in the
        config file, or improperly formatted config.ini.

        INPUT:
        message : string
            String with all error messages contained. Each error is separated
            by the newline character (\n), and thus this string is simply
            displayed to the user in the error window.
        """
        
        # Display Errors
        message = "".join([message,
                           "\nPlease correct these errors and try again.",
                           "\nJRJ will now close."])
        tk.messagebox.showerror(title = "Initialization Error",
                                message = message,
                                parent = self.window)
        
        # Close JRJ
        self.close()
    
    
    def close(self):
        """
        Handles full closure of the JRJ window.
        """
        
        self.window.quit()
        self.window.destroy()
# ----------------------------------------------------------------------------
