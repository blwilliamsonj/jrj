# ----------------------------------------------------------------------------
# juiceboxxxxxx
# 
# --|run.py|------------------------------------------------------------------
# run.py handles starting up the entire solarsim program.
#-----------------------------------------------------------------------------

# --|Imports|-----------------------------------------------------------------
import sys
import jrj
sys.path.insert(0, jrj.RESDIR)
import ctypes
from os.path import exists


# --|Main Method|-------------------------------------------------------------
def startGUI():
    """
    startGUI handles setting the DPI awareness, reading the config file,
    checking for config errors, checking for missing files, and starting the
    JRJ window.
    """
    
    # ----|Set DPI Awareness|----
    try: # Win 8.1 >
        ctypes.windll.shcore.SetProcessDpiAwareness(2)
    except:
        try: # Win 8.0 <
            ctypes.windll.user32.SetProcessDPIAware()
        except:
            pass
    
    
    # ----|Read User Config|----
    config = getConfig()
    
    # ----|Create JRJ object|----
    jrj.JRJ(config)


# --|Helper Methods|----------------------------------------------------------
def getConfig():
    """
    getConfig reads config.ini to get the user's configuration for the JRJ
    window.

    OUTPUT:
    config : dictionary
        config is a dictionary with the various configuration variables from
        config.ini as keys, and their read in values as values.
    """
    
    # ----|Define Useful Variables|----
    lineNo = 0
    config = dict()
    
    # ----|Check for missing files|----
    errors = checkFiles()
    
    # ----|Read config.ini and create config dictionary|----
    # Wrap in try/except to detect errors
    try:
        with open(jrj.CONFIG_FILE, 'r') as file:
            for line in file:
                lineNo += 1
                
                if not line[0] in ["+", "\n"]:
                    # Find index of =
                    eqInd = line.index("=")
                    
                    # Create key:value pair
                    config[line[:eqInd].strip()] = line[eqInd + 1:].strip()
    except:
        # Error while reading config.ini
        config["error"] = " ".join(["Error reading config.ini in line",
                                    lineNo])
    
    # ----|Check config created correctly|----
    errors += checkConfig(config) + checkConfigVals(config)
    
    # Return the config dictionary
    if errors:
        config["error"] = "".join(errors)
    
    return config


def checkConfig(config):
    """
    checkConfig is a helper function to getConfig that detects any missing
    configuration items in config.ini. This helps the user determine if they
    have accidentally deleted anything from the config file that they may need
    to re-add.

    INPUT:
    config : dictionary
        config is a dictionary with the various configuration variables from
        config.ini as keys, and their read in values as values.

    OUTPUT:
    errors : list
        errors contains each missing config value as a string in the list.
        Each string ends in a newline (\n) for easy output later.
        An empty list indicates no errors.
    """
    
    # ----|Check for missing items|----
    errors = [" ".join([item, "property not found in config.ini\n"])
              for item in jrj.CONFIG_ITEMS
              if item not in config.keys()]
    
    return errors


def checkConfigVals(config):
    """
    checkConfigVals is a helper function to getConfig that detects invalid
    configuration values entered by the user in config.ini. Any values that are
    invalid are then shown to the user to help the user correct input errors.

    INPUT:
    config : dictionary
        config is a dictionary with the various configuration variables from
        config.ini as keys, and their read in values as values.

    OUTPUT:
    errors : list
        errors contains each missing config value as a string in the list.
        Each string ends in a newline (\n) for easy output later.
        An empty list indicates no errors.
    """
    
    # ----|Check for Invalid Entries|----
    errors = []
    validColorChars = "0123456789ABCDEF"
    for item in jrj.CONFIG_ITEMS:
        # Check valid integers
        if item not in ["window_title", "window_bg_color"]:
            try:
                if "window" not in item:
                    config[item] = int(config[item])
            except ValueError:
                errors.append(" ".join(["Invalid", item, "value\n"]))
            except KeyError:
                # checkConfig will have already caught this issue
                pass
        
        # Check valid hex code
        if item == "window_bg_color":
            if config[item][0] != "#":
                errors.append("Invalid Window Background Color\n")
            
            config[item] = config[item].upper()
            for char in config[item][1:]:
                if char not in validColorChars:
                    errors.append("Invalid Window Background Color\n")
    
    return errors


def checkFiles():
    """
    checkFiles is a helper function to startGUI to ensure that all files exist
    for the program to use.

    OUTPUT:
    errors : list
        List of strings containing an error message for each missing file.
    """
    
    errors = []
    
    # ----|Check for missing files|----
    # Config File
    errors += ([" ".join([jrj.CONFIG_FILE.replace(jrj.PROJDIR, ""),
                          "not found in main directory\n"])]*
               (not exists(jrj.CONFIG_FILE)))
    
    # Image Files
    errors += [" ".join([file.replace(jrj.RESDIR, ""),
                         "not found in res directory\n"])
               for file in jrj.FILES
               if (file != jrj.CONFIG_FILE and not exists(file))]
    
    return errors


# --|Start main method|-------------------------------------------------------
if __name__ == "__main__":
    startGUI()
# ----------------------------------------------------------------------------
